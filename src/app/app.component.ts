import { NGXLogger } from 'ngx-logger';

import { Component } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Title } from '@angular/platform-browser';

import { BaseComponent } from './shared/components/base/base-component';
import { DataHandlerService } from './shared/services/handler/data-handler.service';
import { ConfigModel } from './shared/models/config-model';
import { ConfigLoaderService } from './shared/services/config/config-loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends BaseComponent {
  config: ConfigModel;
  constructor(
    protected snackBar: MatSnackBar,
    protected logger: NGXLogger,
    protected title: Title,
    public media: MediaObserver,
    public dataHandler: DataHandlerService,
    configLoader: ConfigLoaderService
  ) {
    super('AppComponent', snackBar, logger, title);
    this.config = configLoader.getConfigData();
    this.dataHandler.projectName = this.config['PROJECT_NAME'];
  }

}
