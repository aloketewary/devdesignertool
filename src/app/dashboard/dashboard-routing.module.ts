import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DashboardResolver } from './resolvers/dashboard.resolver';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    resolve: { dashBoardData: DashboardResolver }
  },
  { path: 'icons', loadChildren: 'src/app/icons-base/icons-base.module#IconsBaseModule' },
  { path: 'colors', loadChildren: 'src/app/color-base/color-base.module#ColorBaseModule' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
