import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { DashboardModel } from '../models/dashboard-model';
import { DashboardService } from '../services/dashboard.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardResolver implements Resolve<DashboardModel> {

  constructor(
    private dashboard: DashboardService
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    return this.dashboard.fetchDashBoardData<DashboardModel>();
  }

}
