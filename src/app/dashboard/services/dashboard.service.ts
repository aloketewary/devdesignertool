import { NGXLogger } from 'ngx-logger';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

import { CommonResponse } from '../../shared/models/common-response';
import { ConfigModel } from '../../shared/models/config-model';
import { ConfigLoaderService } from '../../shared/services/config/config-loader.service';
import { AbstractHttpService } from '../../shared/services/http/abstract-http.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService extends AbstractHttpService {
  private config: ConfigModel;
  constructor(
    protected http: HttpClient,
    protected logger: NGXLogger,
    protected snackBar: MatSnackBar,
    configLoader: ConfigLoaderService
  ) {
    super('DashboardService', http, logger, snackBar);
    this.config = configLoader.getConfigData();
  }

  fetchDashBoardData<T>(): Observable<T[]> {
    const url = `${this.config['API_DASHBOARD_DATA_URI']}`;
    return this.http.get<CommonResponse<T>>(url).pipe(
      map((v: CommonResponse<T>) => {
        if (v.status) {
          return v.result as unknown as T[];
        }
        this.showMessage(v.message);
        return null;
      }),
      catchError(this.handleError<T[]>(url))
    );
  }
}
