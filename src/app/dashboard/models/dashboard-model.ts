export class DashboardModel {
  id: string;
  name: string;
  count: string;
  icon: string;
  css: string;
  link: string;
}
