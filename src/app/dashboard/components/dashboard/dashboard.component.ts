import { Component, OnInit } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { NGXLogger } from 'ngx-logger';
import { DataHandlerService } from 'src/app/shared/services/handler/data-handler.service';
import { BaseComponent } from '../../../shared/components/base/base-component';
import { DashboardModel } from '../../models/dashboard-model';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends BaseComponent implements OnInit {
  dashboardDataList: Array<DashboardModel>;

  constructor(
    private route: ActivatedRoute,
    protected snackBar: MatSnackBar,
    protected logger: NGXLogger,
    protected title: Title,
    private router: Router,
    public media: MediaObserver,
    private dataHandler: DataHandlerService
  ) {
    super('DashboardComponent', snackBar, logger, title);
    this.dashboardDataList = this.route.snapshot.data.dashBoardData || [];
  }

  ngOnInit() {
    this.setTitle(`Welcome to Dashboard of ${this.dataHandler.projectName}`);
  }

  gotoRoute(dashData: DashboardModel) {
    this.router.navigate([dashData.link]);
  }
}
