import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataHandlerService {
  private _fatalErrorMessage: string;
  private _projectName: string;

  constructor() { }

  public get fatalErrorMessage(): string {
    return this._fatalErrorMessage;
  }
  public set fatalErrorMessage(value: string) {
    this._fatalErrorMessage = value;
  }

  public get projectName(): string {
    return this._projectName;
  }
  public set projectName(value: string) {
    this._projectName = value;
  }
}
