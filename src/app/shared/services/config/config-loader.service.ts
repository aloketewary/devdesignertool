import { NGXLogger } from 'ngx-logger';
import { environment } from 'src/environments/environment';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { CommonResponse } from '../../models/common-response';
import { ConfigModel } from '../../models/config-model';

@Injectable({
  providedIn: 'root'
})
export class ConfigLoaderService {
  private config: ConfigModel;
  constructor(private http: HttpClient, private logger: NGXLogger) {
    this.config = new ConfigModel();
  }

  public getConfigData(): ConfigModel {
    return this.config;
  }

  load(): Promise<any> {
    this.logger.debug(`[ ConfigLoaderService ] getSettings:: from server`);
    const promise = this.http.get(`${environment.endpoint}/config`)
      .toPromise()
      .then((data: CommonResponse<ConfigModel>) => {
        if (data.status) {
          return this.config = data.result;
        }
        this.logger.error(`[ ConfigLoaderService ] getSettings:: unable to get call`);
        return data.message;
      });
    return promise;
  }
}
