import { NGXLogger } from 'ngx-logger';
import { Observable, of } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class AbstractHttpService {
  constructor(
    protected className: string,
    protected http: HttpClient,
    protected logger: NGXLogger,
    protected snackBar: MatSnackBar
  ) {
    this.className = className;
  }

  protected handleError<T>(uri: string, result?: T) {
    return (error: any): Observable<T> => {
      this.logger.error(`[ ${this.className} ] http request failed:`, uri);
      this.logger.error(`[ ${this.className} ] error`, error);
      this.logger.error(`[ ${this.className} ] response`, JSON.stringify(result));
      this.showMessage(`http request failed`);
      return of(result as T);
    };
  }

  showMessage(message: string) {
    const config: MatSnackBarConfig = {
      verticalPosition: 'top',
      horizontalPosition: 'right',
      duration: 5000,
    };
    this.snackBar.open(message || '', '', config);
  }
}
