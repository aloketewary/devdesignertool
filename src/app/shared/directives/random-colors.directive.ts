import { Directive, OnInit, Input, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appRandomColors]'
})
export class RandomColorsDirective implements OnInit {

  @Input('appRandomColors') colors: string[];
  constructor(private elRef: ElementRef,
    private renderer: Renderer2) { }

  ngOnInit(): void {
    const COLORS = this.colors || ['bg-teal-gradient', 'bg-red-gradient', 'bg-green-gradient', 'bg-amber-gradient', 'bg-blue-gradient'];
    const randomNumber = Math.floor(Math.random() * COLORS.length);
    this.renderer.addClass(this.elRef.nativeElement, `${COLORS[randomNumber]}`);
  }

}
