export class Utility {
  public static loadExternalStyles(styleUrl: string) {
    return new Promise((resolve, reject) => {
      const styleElement = document.createElement('link');
      styleElement.href = styleUrl;
      styleElement.onload = resolve;
      styleElement.rel = 'stylesheet';
      document.head.appendChild(styleElement);
    });
  }

  public static isNullOrUndefined(val: any): boolean {
    return (val === null || val === undefined);
  }
}
