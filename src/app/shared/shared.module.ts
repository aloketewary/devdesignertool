import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';

import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_BOTTOM_SHEET_DEFAULT_OPTIONS, MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule, MatIconRegistry } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { DomSanitizer } from '@angular/platform-browser';

import { LoaderComponent } from './components/loader/loader.component';
import { MultiErrorPageComponent } from './components/multi-error-page/multi-error-page.component';
import { RandomColorsDirective } from './directives/random-colors.directive';
import { SharedRoutingModule } from './shared-routing.module';

@NgModule({
  declarations: [RandomColorsDirective, LoaderComponent, MultiErrorPageComponent],
  imports: [
    CommonModule,
    SharedRoutingModule,
    HttpClientModule,
    MatSnackBarModule,
    LoggerModule.forRoot({ level: NgxLoggerLevel.TRACE }),
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    FlexLayoutModule,
    MatSidenavModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatBottomSheetModule,
    MatListModule,
    MatDividerModule
  ],
  exports: [
    MatSnackBarModule,
    LoggerModule,
    HttpClientModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    FlexLayoutModule,
    RandomColorsDirective,
    LoaderComponent,
    MatSidenavModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatBottomSheetModule,
    MatListModule,
    MatDividerModule
  ],
  providers: [
    { provide: MAT_BOTTOM_SHEET_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } }
  ]
})
export class SharedModule {
  relLocation = '../../assets';
  constructor(iconReg: MatIconRegistry, santize: DomSanitizer) {
    iconReg.addSvgIconSet(santize.bypassSecurityTrustResourceUrl(`${this.relLocation}/icons/mdi.svg`));
    iconReg.addSvgIcon('icon-logo', santize.bypassSecurityTrustResourceUrl(`${this.relLocation}/icons/custom/small_icon.svg`));
    iconReg.addSvgIcon('color-logo', santize.bypassSecurityTrustResourceUrl(`${this.relLocation}/icons/custom/color_swatch.svg`));
    iconReg.addSvgIcon('palette-logo', santize.bypassSecurityTrustResourceUrl(`${this.relLocation}/icons/custom/palette.svg`));
    iconReg.addSvgIcon('fonts-logo', santize.bypassSecurityTrustResourceUrl(`${this.relLocation}/icons/custom/fonts.svg`));
  }
}
