import { NGXLogger } from 'ngx-logger';

import { OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { Title } from '@angular/platform-browser';

export class BaseComponent implements OnInit {
  constructor(
    protected className: string,
    protected snackBar: MatSnackBar,
    protected logger: NGXLogger,
    protected title: Title
  ) {
    this.className = className;
  }

  ngOnInit() {
    // this.logger.debug('Your log message goes here');
  }

  protected setTitle(title: string) {
    this.title.setTitle(title);
  }

  protected copyMessage(val: string) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.openSnackBar('Copy Successfully to the clipboard!');
  }

  protected openSnackBar(msg: string) {
    const dataConfig: MatSnackBarConfig = {
      verticalPosition: 'bottom',
      horizontalPosition: 'center',
      duration: 4000
    };
    this.snackBar.open(msg, '', dataConfig);
  }
}
