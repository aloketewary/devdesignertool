import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiErrorPageComponent } from './multi-error-page.component';

describe('MultiErrorPageComponent', () => {
  let component: MultiErrorPageComponent;
  let fixture: ComponentFixture<MultiErrorPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiErrorPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiErrorPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
