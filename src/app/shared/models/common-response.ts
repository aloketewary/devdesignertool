export class CommonResponse<T> {
  status: boolean;
  result: T;
  message: string;
}
