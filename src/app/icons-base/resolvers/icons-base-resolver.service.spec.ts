import { TestBed } from '@angular/core/testing';

import { IconsBaseResolverService } from './icons-base-resolver.service';

describe('IconsBaseResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IconsBaseResolverService = TestBed.get(IconsBaseResolverService);
    expect(service).toBeTruthy();
  });
});
