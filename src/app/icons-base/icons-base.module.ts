import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { IconsComponent } from './components/icons/icons.component';
import {
    IndividualIconsBottomSheetsComponent
} from './components/individual-icons-bottom-sheets/individual-icons-bottom-sheets.component';
import { IconsBaseRoutingModule } from './icons-base-routing.module';

@NgModule({
  declarations: [IconsComponent, IndividualIconsBottomSheetsComponent],
  imports: [
    CommonModule,
    IconsBaseRoutingModule,
    SharedModule
  ],
  entryComponents: [IndividualIconsBottomSheetsComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class IconsBaseModule { }
