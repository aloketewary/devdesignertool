import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndividualIconsBottomSheetsComponent } from './individual-icons-bottom-sheets.component';

describe('IndividualIconsBottomSheetsComponent', () => {
  let component: IndividualIconsBottomSheetsComponent;
  let fixture: ComponentFixture<IndividualIconsBottomSheetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndividualIconsBottomSheetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndividualIconsBottomSheetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
