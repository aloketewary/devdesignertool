import { NGXLogger } from 'ngx-logger';

import { Component, OnInit } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Title } from '@angular/platform-browser';

import { BaseComponent } from '../../../shared/components/base/base-component';
import { BrowserUsageCode } from '../../models/browser-usage-code';
import { GenericIconModel } from '../../models/generic-icon-model';

@Component({
  selector: 'app-individual-icons-bottom-sheets',
  templateUrl: './individual-icons-bottom-sheets.component.html',
  styleUrls: ['./individual-icons-bottom-sheets.component.scss']
})
export class IndividualIconsBottomSheetsComponent extends BaseComponent implements OnInit {
  selectedIcon: GenericIconModel;
  iconsFontInstruction: string;
  browserUsageCode: Array<BrowserUsageCode>;
  isAssetsAvailable: boolean;
  constructor(private bottomSheetRef: MatBottomSheetRef<IndividualIconsBottomSheetsComponent>,
    protected snackBar: MatSnackBar,
    protected logger: NGXLogger,
    protected title: Title) {
    super('IndividualIconsBottomSheetsComponent', snackBar, logger, title);
    this.browserUsageCode = new Array<BrowserUsageCode>(0);
  }

  ngOnInit() {
  }

  copyMessageData(bUC: BrowserUsageCode) {
    this.copyMessage(bUC.htmlString);
    this.bottomSheetRef.dismiss();
  }
}
