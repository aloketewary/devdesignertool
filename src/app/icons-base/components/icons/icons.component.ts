import { NGXLogger } from 'ngx-logger';

import { animate, query, stagger, state, style, transition, trigger } from '@angular/animations';
import { Location } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

import { BaseComponent } from '../../../shared/components/base/base-component';
import { ConfigModel } from '../../../shared/models/config-model';
import { ConfigLoaderService } from '../../../shared/services/config/config-loader.service';
import { Utility } from '../../../shared/utils/utility';
import { AvailableIconModel } from '../../models/available-icon-model';
import { BrowserType, BrowserUsageCode } from '../../models/browser-usage-code';
import {
    CategoryForGenIconModel, GenCatIconModel, GenericIconModel
} from '../../models/generic-icon-model';
import { IconListModel } from '../../models/icon-list-model';
import { IconNameModel } from '../../models/icon-name-model';
import { IconsDbService } from '../../services/icons-db.service';
import {
    IndividualIconsBottomSheetsComponent
} from '../individual-icons-bottom-sheets/individual-icons-bottom-sheets.component';

@Component({
  selector: 'app-icons',
  templateUrl: './icons.component.html',
  styleUrls: ['./icons.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
    trigger('listAnimation', [
      transition('* => *', [
        query(':enter', style({ opacity: 0 }), { optional: true }),
        query(':enter', [
          style({ opacity: 0 }),
          stagger(100, [
            animate('0.5s', style({ opacity: 1 }))
          ])
        ], { optional: true })
      ])
    ])
  ]
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconsComponent extends BaseComponent implements OnInit {

  searchForm: FormGroup;
  private _searchVal: string;
  availableIconsList: Array<AvailableIconModel>;
  config: ConfigModel;
  selectedIconData: AvailableIconModel;
  isServerQueryRunning: boolean;
  iconsList: Array<GenCatIconModel>;
  iconsName: Array<IconNameModel>;
  constructor(
    private _fBuilder: FormBuilder,
    private route: ActivatedRoute,
    private location: Location,
    configLoader: ConfigLoaderService,
    protected title: Title,
    protected snackBar: MatSnackBar,
    protected logger: NGXLogger,
    private iconsDb: IconsDbService,
    private cdr: ChangeDetectorRef,
    private bottomSheet: MatBottomSheet
  ) {
    super('IconsComponent', snackBar, logger, title);
    this.config = configLoader.getConfigData();
    this.availableIconsList = this.config['AVAILABLE_ICONS'] || new Array<AvailableIconModel>(0);
    this.selectedIconData = new AvailableIconModel();
    this.iconsList = new Array<GenCatIconModel>(0);
    this.iconsName = this.config['ICON_NAME_LIST'];
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.searchVal = params.s;
    });
    this._initForm();
    if (this.availableIconsList.length > 0) {
      this.selectedIconData = this.availableIconsList[0];
      this.fetchServerData(this.selectedIconData);
    }
  }

  private _initForm() {
    this.searchForm = this._fBuilder.group({
      search: new FormControl(this.searchVal, Validators.compose([])),
    });
  }

  goback() {
    this.location.back();
  }

  public get searchVal(): string {
    return this._searchVal;
  }
  public set searchVal(value: string) {
    this._searchVal = value;
  }

  public searchEntered() {
    this.searchForm.controls['search'].valueChanges.subscribe(v => console.log(v));
  }

  public voiceCommands() {

  }

  public selectedIconsData(iconData: AvailableIconModel) {
    this.selectedIconData = iconData;
    const selectedIcon = (this.config['ICONS_LIST'] as Array<IconListModel>).find(icon => icon.name === iconData.short_name);
    if (!Utility.isNullOrUndefined(selectedIcon) && !selectedIcon.loaded) {
      if (!Utility.isNullOrUndefined(selectedIcon.styleLink)) {
        Utility.loadExternalStyles(selectedIcon.styleLink).then(
          v => {
            selectedIcon.loaded = true;
            this.logger.debug(`[ ${this.className} ] : Link loaded`);
          }
        ).catch(v => this.logger.error(`[ ${this.className} ] : Link loaded error`));
      }
    }
    this.fetchServerData(iconData);
  }

  public fetchServerData(iconData: AvailableIconModel) {
    this.isServerQueryRunning = true;
    const selectedIcon = (this.config['ICONS_LIST'] as Array<IconListModel>).find(icon => icon.name === iconData.short_name);
    this.iconsName.forEach((val, idx) => {
      if (selectedIcon.name === val.name) {
        this.iconsDb.fetchIconsData<GenCatIconModel, CategoryForGenIconModel>(
          selectedIcon.iconListUrl, iconData.short_name, val.rootSelector).subscribe(v => {
            if (!Utility.isNullOrUndefined(v)) {
              v.forEach(element => {
                this.iconsList.push(element);
              });
              this.cdr.detectChanges();
            }
            this.isServerQueryRunning = false;
          });
      }
    });
  }

  openBottomSheetForIcons(selectedIcon: GenericIconModel) {
    const iconBtmSheet = this.bottomSheet.open(IndividualIconsBottomSheetsComponent, {
      hasBackdrop: true,
      disableClose: false
    });
    iconBtmSheet.instance.selectedIcon = selectedIcon;
    // tslint:disable-next-line:max-line-length
    iconBtmSheet.instance.iconsFontInstruction = `<span>Follow the <a href="http://google.github.io/material-design-icons/" target="_blank">instructions</a> to embed the icon font in your site and learn how to style your icons using CSS.</span>`;
    iconBtmSheet.instance.browserUsageCode.push(new BrowserUsageCode(
      `<i class="material-icons">${selectedIcon.ligature}</i>`, selectedIcon.name, selectedIcon.ligature, BrowserType.MODERN
    ));
    iconBtmSheet.instance.browserUsageCode.push(new BrowserUsageCode(
      `<i class="material-icons">${selectedIcon.ligature}</i>`, selectedIcon.name, selectedIcon.ligature, BrowserType.OLD
    ));
    iconBtmSheet.instance.browserUsageCode.push(new BrowserUsageCode(
      `${selectedIcon.ligature}`, selectedIcon.name, selectedIcon.ligature, BrowserType.ICON_ONLY
    ));
    iconBtmSheet.instance.isAssetsAvailable = false;
    iconBtmSheet.afterDismissed().subscribe(result => {
      console.log(result);
    });
  }
}
