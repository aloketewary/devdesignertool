import { TestBed } from '@angular/core/testing';

import { IconsDbService } from './icons-db.service';

describe('IconsDbService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IconsDbService = TestBed.get(IconsDbService);
    expect(service).toBeTruthy();
  });
});
