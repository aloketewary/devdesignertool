import { NGXLogger } from 'ngx-logger';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

import { AbstractHttpService } from '../../shared/services/http/abstract-http.service';

@Injectable({
  providedIn: 'root'
})
export class IconsDbService extends AbstractHttpService {

  constructor(
    protected http: HttpClient,
    protected logger: NGXLogger,
    protected snackBar: MatSnackBar
  ) {
    super('IconsDbService', http, logger, snackBar);
  }

  /**
   * ftech the data of icons from shared source
   * @param url url to fetch the data
   * @param _for for which category
   */
  fetchIconsData<T, E>(url: string, _for: string, key: string): Observable<T[]> {
    return this.http.get<E>(url).pipe(
      map((v: E) => {
        if (v && _for === v['valueFor']) {
          return v[key] as unknown as T[];
        }
        this.showMessage('This icons can\'t loaded right now');
        return null;
      }),
      tap((t: T[]) => this.logger.log(`Icons Data loaded`)),
      catchError(this.handleError<T[]>(url))
    );
  }
}
