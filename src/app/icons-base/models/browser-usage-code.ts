export class BrowserUsageCode {
  htmlString: string;
  iconName: string;
  iconLigature: string;
  forBrowser: BrowserType;

  constructor(_htmlString?: string, _iconName?: string, _iconLigature?: string, _forBrowser?: BrowserType) {
    this.htmlString = _htmlString;
    this.iconName = _iconName;
    this.iconLigature = _iconLigature;
    this.forBrowser = _forBrowser;
  }
}

export enum BrowserType {
  OLD = 'For IE9 or below.',
  MODERN = 'For modern browsers',
  ICON_ONLY = 'Icon name only'
}
