export class IconNameModel {
  name: string;
  rootSelector: string;
  id: number;
}
