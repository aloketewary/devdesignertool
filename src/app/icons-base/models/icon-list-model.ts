export class IconListModel {
  name: string;
  cssStyle: string;
  styleLink: string;
  cssDirective: string;
  loaded: boolean;
  iconListUrl: string;
}
