export class GenericIconModel {
  codepoint: string;
  ligature: string;
  keywords: Array<string>;
  group_id: string;
  name: string;
  id: string;

  constructor() {
    this.keywords = new Array<string>(0);
  }
}

export class CategoryForGenIconModel {
  categories: Array<GenCatIconModel>;
  valueFor: string;
  constructor() {
    this.categories = new Array<GenCatIconModel>(0);
  }
}

export class GenCatIconModel {
  icons: Array<GenericIconModel>;
  name: string;
  key: string;
  constructor() {
    this.icons = new Array<GenericIconModel>(0);
  }
}
