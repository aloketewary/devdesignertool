export class AvailableIconModel {
  short_name: string;
  icon: string;
  author: string;
  desc: string;
  name: string;
  disable: boolean;
}
