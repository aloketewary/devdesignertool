import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
    MultiErrorPageComponent
} from './shared/components/multi-error-page/multi-error-page.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', loadChildren: 'src/app/dashboard/dashboard.module#DashboardModule' },
  {
    path: '**', component: MultiErrorPageComponent, pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
