import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from './../shared/shared.module';
import { ColorBaseRoutingModule } from './color-base-routing.module';
import { ColorsComponent } from './components/colors/colors.component';

@NgModule({
  declarations: [ColorsComponent],
  imports: [
    CommonModule,
    ColorBaseRoutingModule,
    SharedModule
  ]
})
export class ColorBaseModule { }
