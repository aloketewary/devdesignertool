import { Component, OnInit } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    public media: MediaObserver,
    private router: Router
  ) {
    this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        const activeUrl = e.urlAfterRedirects || e.url;
        if (activeUrl === '/home') {
          this.router.navigateByUrl('/home/dashboard');
        }
      }
    });
  }

  ngOnInit() {
  }

}
